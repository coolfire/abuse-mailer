#!/usr/bin/env ruby

require 'date'
require 'ipaddr'

# Some configuration parameters
fw_logfile  = '/home/coolfire/fw_alerts.log'
f2b_logfile = '/var/log/fail2ban.log'

home_net_4  = IPAddr.new '91.134.247.202/32'
home_net_6  = IPAddr.new '2001:470:1f15:958::2:1/64' # Currently unused

cc_address  = 'coolfire@insomnia247.nl'

iplist      = Array.new

# Get the date
date  = Date.today.prev_day
zone  = Time.now.zone

fw_date  = date.strftime('%m/%d/%Y')
f2b_date = date.strftime('%Y-%m-%d')

# Parse suricata firewall log
fh = File.open(fw_logfile, 'r')

fh.each_line do |line|
	# Check if it's a relevant entry
	if(line =~ /(heartbleed|brute|Priority: 1)/i)

		# Parse out variables
		chunks = line.split('[')
		date   = chunks[0]
		ip     = chunks[5].split('}')[1].split('->')[0].split(':')
		ip     = ip.first ip.size - 1
		ip     = ip.join(':').strip
		ip     = IPAddr.new ip

		# Make sure we don't report ourself
		if( home_net_4.include?( ip ) || home_net_6.include?( ip ) )
			next
		end

		# Check if line from yesterday
		if(date =~ /^#{fw_date}/)

			# Check if we have this IP in the list already
			if(!iplist.include?(ip))
				iplist.push(ip)
			end
		end
	end
end

fh.close

# Parse Fail2ban log
fh = File.open(f2b_logfile, 'r')

fh.each_line do |line|
	# Check if it's a ban action
	if(line =~ /Ban /)

		# Parse out variables
		chunks = line.split(' ')
		date   = chunks[0]
		ip     = chunks[6]
		ip     = IPAddr.new ip
	end

	# Check if line from yesterday
	if(date =~ /^#{f2b_date}/)

	# Check if we have this IP in the list already
                        if(!iplist.include?(ip))
                                iplist.push(ip)
                        end
	end
end

fh.close

# Process list of agragated IP addresses
iplist.each do |ip|

	if(!ip.nil?)
		# Build DNS request
		rev     = ip.reverse
		dns_req = rev.gsub(/(\.in-addr\.arpa|\.ip6.arpa)/, '.abuse-contacts.abusix.org')

		# Request abuse contact address from abusix.org
		abuse_contact = %x(dig -t TXT +short #{dns_req}).gsub!(/"/, '')
		
		# Check if we have in fact receive an abuse contact
		if(!abuse_contact.nil? && abuse_contact != "")

			# Strip newline
			abuse_contact.gsub!(/\n/, '')

			# Gather details about the IP
			# From suricata
			fw_details = %x(grep #{ip.to_string} #{fw_logfile} | grep #{fw_date})

			# From Fail2ban
			f2b_details = %x(grep #{ip.to_string} #{f2b_logfile} | grep #{f2b_date})

			# Build mail body
			mail = <<MESSAGE_END
Dear #{abuse_contact},

We wish to inform we have detected an attacked from the following IP address:
#{ip.to_s}
At the bottom of this email you will find more details about this attack.
If you believe this was misclassified as an attack please contact us so we can verify this and adjust our detection.

This email was automatically generated. You are receiving this email because you are listed as the abuse contact for this IP address. If this is incorrect please contact your own IT department to have this corrected.

Kind regards,
The Insomnia 24/7 team



Target server addresses:
91.134.247.202

Target server timezone:
#{zone}

Suricata hits:
#{fw_details}

Fail2ban hits:
#{f2b_details}
MESSAGE_END

			# Send out mail
			%x(echo '#{mail}' | mail -s "Abuse from #{ip.to_s}" -c "#{cc_address}" "#{abuse_contact}" )

			# Don't batter our upstram mail servers
			sleep(2)
		end
	end
end
